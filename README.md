Pickles 2 - MODULE SET SAMPLE
=========

[Pickles 2](http://pickles2.pxt.jp/) のドキュメントモジュールセットのサンプルです。



## 導入方法 - Setup

### 1. [Pickles 2](http://pickles2.pxt.jp/) をセットアップ

### 2. composer.json に、パッケージ情報を追加

```
{
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/tomk79/sample-modules-001.git"
        }
    ],
    "require": {
        "tomk79/px2-pickles2-sample-modules-001": "dev-master"
    }
}
```

### 3. composer update

更新したパッケージ情報を反映します。

```
$ composer update
```

### 4. config.php を更新

モジュール設定を書き加えます。

```
<?php
return call_user_func( function(){

	/* (中略) */

	@$conf->plugins->px2dt->paths_module_template["ModuleSetSample"] = "./vendor/tomk79/px2-pickles2-sample-modules-001/modules/";

	/* (中略) */

	return $conf;
} );
```



## ライセンス - License

MIT License


## 作者 - Author

- (C)Tomoya Koyanagi <tomk79@gmail.com>
- website: <http://www.pxt.jp/>
- Twitter: @tomk79 <http://twitter.com/tomk79/>


